# Contributing workflows

This guide was started during the OpenFOAM RSE meeting of July 28, 2023.
However, it is not meant to be static: Contribute your knowledge via a merge request!

## Branching models

- main branch: releases
- develop branch: where new features get merged
- feature branches: one per each feature / self-contained contribution (one feature can have several branches that lead to it)
- hotfixes

Example: [Git Flow](https://nvie.com/posts/a-successful-git-branching-model/) - see [more examples from the Simulation Software Engineering lecture](https://github.com/Simulation-Software-Engineering/Lecture-Material/blob/main/01_version_control/workflow_slides.md)

## General workflows

### Example: OpenFOAM

Code: [hosted GitLab](https://develop.openfoam.com/) and [GitLab.com](https://gitlab.com/openfoam)

- Custom regression testing tools, now some adoption of GitLab CI
   - Restriction: Also confidential test cases, cannot have it public
   - Tests include verification and validation cases
- Continuous integration workflows: Wishlist, but few developers
- Small developers team (4-5x regular developers), code reviews mostly synchronous
- Three roles:
   - Merge Request submitter: Describes the contribution and finds reviewers
   - Reviewer: Discusses the contribution and does some testing
   - Assignee: Does final testing and decides to merge
- Also: Human-driven testing
- All code contributions are public

### Example: foam-extend

Code: [SourceForge](https://sourceforge.net/p/foam-extend/foam-extend-5.0/ci/master/tree/), for historical reasons

- Small developers team (2x regular developers), code reviews both synchronously and asynchronously
- No automation for the tests

### Example: exaFOAM

Code: [hosted GitLab](https://develop.openfoam.com/exafoam/) (reduced foam-extend + research and development of features)

- Small developers team (2x regular developers)

### Example: preCICE

Code: Multiple repositories on [GitHub](https://github.com/precice)

- Developers are PhD students (~7), of different contribution levels
- Pull requests pre-fill a template, which includes a checklist for the contributor and the reviewer
- Code reviews are mostly asynchronous, on GitHub ([example](https://github.com/precice/precice/pull/1742))
- Several automatic tests in the core library, fewer tests in other components: formatting, C++ linting, unit/integration tests, ... (also described in [this paper](https://open-research-europe.ec.europa.eu/articles/2-51/v2))
- Distributed repositories (instead of monorepo): isolated damage, easier onboarding on specific components


### Examples from BitBucket

(none - please add some from your project)

## Guidelines for successful merge requests

(add yours)
